class CategoriesController < ApplicationController
  def index
  end

  def show
    @category = Category.find(params[:id])
    @notices = @category.notices.order(created_at: :desc)
  end

  def new
  end

  def edit
  end
end

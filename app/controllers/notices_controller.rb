class NoticesController < ApplicationController

  before_action :correct_user, only: [:edit, :update, :destroy]

  def index
    @notices = Notice.order(created_at: :desc)
  end

  def show
    @notice = Notice.find(params[:id])
  end

  def new
    @notice = Notice.new
  end

  def create
    @notice = current_user.notices.build(notice_params)

    if @notice.save
      flash[:success] = 'Your notice has been created successfully'
      redirect_to root_url
    else
      render 'new'
    end
  end

  def edit
  end

  def update

  end

  def destroy

  end

  private

  def notice_params
    params.require(:notice).permit(:category_id, :title, :description, :contacts)
  end

  def correct_user
    @notice = Notice.find(params[:id])
    unless @notice.user == current_user
      flash[:danger] = 'This is not your notice.'
      redirect_to root_url
    end
  end
end

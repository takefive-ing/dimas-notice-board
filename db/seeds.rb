# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

categories = []

categories.push Category.create(name: 'Need help')
categories.push Category.create(name: 'Buy and sell')
categories.push Category.create(name: 'Dating')
categories.push Category.create(name: 'Work')
categories.push Category.create(name: 'Misc')

users = []
3.times do
  name = Faker::Name.name
  users.push User.create(name: name, email: Faker::Internet.email(name), password: '12345678', password_confirmation: '12345678')
end

5.times do
  Notice.create(title: Faker::Lorem.sentence,
                description: Faker::Lorem.paragraph,
                contacts: Faker::PhoneNumber.cell_phone,
                category: categories.sample,
                user: users.sample
  )
end